<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 20/01/2018
 * Time: 22:53
 */

namespace Src\Metrics;

use \Src\Utils\Colors;

class Depth
{

    /**
     * @var \Src\Binance\API
     */
    private $api;

    /**
     * @var string
     */
    private $symbol;

    /**
     * @var
     */
    private $rawData;

    /**
     * @var
     */
    private $result;

    /**
     * @var
     */
    private $printResult;

    /**
     *
     */
    const DEPTH_GOAL = 0.35;

    /**
     * @var array
     */
    private $prevResult = [];

    /**
     * @var array|mixed
     */
    private $simulate = [];

    /**
     * Depth constructor.
     * @param \Src\Binance\API $api
     * @param string $symbol
     */
    public function __construct(\Src\Binance\API $api, $symbol = 'BTCETH')
    {
        $this->api = $api;
        $this->symbol = $symbol;
        $this->simulate = include('cli/simdata.php');
    }

    public function checkGoalSimulate()
    {
        if(empty($this->simulate))
        {
            echo PHP_EOL . "STOP" . PHP_EOL;
            die();
        }
        $this->rawData = $this->simulate[key($this->simulate)];
        unset($this->simulate[key($this->simulate)]);
        $this->_calc();

        return $this;
    }

    public function getTestDepth()
    {
        $this->rawData = $this->simulate[key($this->simulate)];
        unset($this->simulate[key($this->simulate)]);
        $this->_calc();

        return $this;
    }

    public function checkGoal()
    {
        $this->getDepth();

        if($this->result['diff'] > self::DEPTH_GOAL)
        {
            return true;
        }

        return false;
    }

    /**
     * @return $this
     */
    public function getDepth()
    {
        $this->rawData = $this->_getDepth();
        $this->_calc();

        return $this;
    }

    /**
     * @return string
     */
    public function printDepth()
    {
        $this->_colorFroCli();
        return sprintf("%s: Bid => %s | Ask => %s | Diff => %s",
            $this->printResult['date'],
            $this->printResult['bid'],
            $this->printResult['ask'],
            $this->printResult['diff']
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $this->_colorFroCli();
        return sprintf("%s: Bid => %s | Ask => %s | Diff => %s",
            $this->printResult['date'],
            $this->printResult['bid'],
            $this->printResult['ask'],
            $this->printResult['diff']
        );
    }

    /**
     * @return void
     */
    private function _calc()
    {
        $this->prevResult = $this->result;

        $this->result = [
            'bid' => \Src\Utils\Number::wrapFloat(key($this->rawData['bids'])),
            'ask' => \Src\Utils\Number::wrapFloat(key($this->rawData['asks'])),
        ];

        $this->result['diff'] = number_format(100 - (100 * ($this->result['bid'] / $this->result['ask'])), 2);

        $this->result['date']  = $this->_getDate();

        $this->printResult = $this->result;

    }

    /**
     * @return string
     */
    private function _getDate()
    {
        $date = \DateTime::createFromFormat('U.u', microtime(true));
        return $date->format("d.m.Y H:i:s.v");
    }

    /**
     * @return array
     */
    private function _getDepth()
    {
        return $this->api->depth($this->symbol);
    }

    /**
     *
     */
    private function _colorFroCli()
    {
        if (php_sapi_name() == "cli") {
            $colors = new Colors();

            $this->printResult['date'] = $colors->getColoredString($this->result['date'], 'yellow');

            $this->printResult['bid'] = $colors->getColoredString($this->result['bid'], 'green');
            $this->printResult['ask'] = $colors->getColoredString($this->result['ask'], 'red');

            if($this->result['ask'] > $this->prevResult['ask'])
            {
                $this->printResult['ask'] = $colors->getColoredString("(+) ", 'green') . $this->printResult['ask'];
            } else if ($this->result['ask'] < $this->prevResult['ask']) {
                $this->printResult['ask'] = $colors->getColoredString("(-) ", 'red') . $this->printResult['ask'];
            }

            if($this->result['bid'] > $this->prevResult['bid'])
            {
                $this->printResult['bid'] = $colors->getColoredString("(+) ", 'green') . $this->printResult['bid'];
            } else if ($this->result['bid'] < $this->prevResult['bid']) {
                $this->printResult['bid'] = $colors->getColoredString("(-) ", 'red') . $this->printResult['bid'];
            }

            if($this->result['diff'] >= self::DEPTH_GOAL)
            {
                $this->printResult['diff'] = $colors->getColoredString(" (+) " . $this->result['diff'], 'green');
            } else {
                $this->printResult['diff'] = $colors->getColoredString(" (-) " . $this->result['diff'], 'red');
            }
        }
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
}