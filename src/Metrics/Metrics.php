<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 13:46
 */

namespace Src\Metrics;


class Metrics
{

    /**
     * @var Depth
     */
    private $depthMetric;

    /**
     * Metrics constructor.
     * @param \Src\Binance\API $api
     * @param $symbol
     */
    public function __construct(\Src\Binance\API $api, $symbol)
    {
        $this->depthMetric = new \Src\Metrics\Depth($api, $symbol);
    }

    /**
     * @return Depth
     */
    public function getDepthMetric()
    {
        return $this->depthMetric;
    }


}