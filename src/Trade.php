<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 00:12
 */

namespace Src;


class Trade
{

    /**
     * @var array
     */
    private $orders = [];

    /**
     * @var float
     */
    private $minimal = 0.00200001;

    /**
     * @var float
     */
    private $fee = 0.01 / 100;

    /**
     * @var null
     */
    private $metric = null;

    /**
     * @var array
     */
    private $math = [];

    public function __construct()
    {

    }

    private function calc()
    {
        $this->math = [
            'buyPrice'  => \Src\Utils\Number::wrapFloat($this->metric->getResult()['bid'] + 0.00000001),
            'sellPrice' => \Src\Utils\Number::wrapFloat($this->metric->getResult()['ask'] - 0.00000001),
            'buySum'    => 0,
            'sellSum'   => 0,
            'buyResult'     => 0,
            'sellResult'    => 0
        ];

        $this->math['buyCount']  = ceil($this->minimal / $this->math['buyPrice']);
        $this->math['sellCount'] = ceil($this->minimal / $this->math['sellPrice']);

        $this->math['buySum']  = \Src\Utils\Number::wrapFloat($this->math['buyPrice'] * $this->math['buyCount']);
        $this->math['sellSum'] = \Src\Utils\Number::wrapFloat($this->math['sellPrice'] * $this->math['sellCount']);

        $this->math['buyResult']  = \Src\Utils\Number::wrapFloat($this->math['buyCount'] - ($this->math['buyCount'] * $this->fee));
        $this->math['sellResult'] = \Src\Utils\Number::wrapFloat($this->math['sellSum'] - ($this->math['sellSum'] * $this->fee));
    }

    /**
     * @param Metrics\Depth $metric
     */
    public function check(\Src\Metrics\Depth $metric, \Src\Binance\Orders $orders, \Src\Binance\Balance $balance)
    {
        $this->metric = $metric;

        if($this->analyze())
        {
            $this->buy($orders, $balance);
            $this->sell($orders, $balance);
        }

        $orders->closeOrders($balance, $this->metric->getResult()['bid'], \Src\Binance\Orders::TYPE_BUY);
        $orders->closeOrders($balance, $this->metric->getResult()['ask'], \Src\Binance\Orders::TYPE_SELL);
        $orders->report();
    }

    private function buy(\Src\Binance\Orders $orders, \Src\Binance\Balance $balance)
    {
        $orders->addOrder($this->math['buyPrice'], $this->math['buyCount'], $this->math['buySum'], $this->math['buyResult'], \Src\Binance\Orders::TYPE_BUY);
    }

    private function sell(\Src\Binance\Orders $orders, \Src\Binance\Balance $balance)
    {
        $orders->addOrder($this->math['sellPrice'], $this->math['sellCount'], $this->math['sellSum'], $this->math['sellResult'], \Src\Binance\Orders::TYPE_SELL);
    }

    /**
     * @return bool
     */
    public function analyze()
    {
        $this->calc();
        return $this->_depthAnalyze($this->metric->getResult()) ? true : false;

    }

    private function _depthAnalyze()
    {
        $result = $this->metric->getResult();
        $depthGoalNumber = \Src\Metrics\Depth::DEPTH_GOAL;

        if($result['diff'] >= $depthGoalNumber)
        {
            return true;
        }

        return false;
    }


    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return array
     */
    public function getMath()
    {
        return $this->math;
    }
}