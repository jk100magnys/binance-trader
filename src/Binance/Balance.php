<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 01:11
 */

namespace Src\Binance;


class Balance
{

    /**
     * @var array
     */
    private $startBalance = [];

    /**
     * @var array
     */
    private $balance = [];

    /**
     * @var \Src\Config\Config
     */
    private $config;

    public function __construct(\Src\Config\Config $config)
    {
        $this->config = $config;
        $this->balance = $this->config->getConfig('balance');
        $this->startBalance = $this->balance;
    }

    /**
     * @param \Src\Metrics\Metrics $metrics
     */
    public function addBalanceForSimulate(\Src\Metrics\Metrics $metrics)
    {
        $mainValue = $this->config->getSecondSymbol();
        $prices = $metrics->getDepthMetric()->getTestDepth()->getResult();
        $price = $prices['ask'];
        $this->balance[$this->config->getFirstSymbol()] = \Src\Utils\Number::wrapFloat($this->balance[$this->config->getSecondSymbol()] / $price);
        $this->startBalance = $this->balance;

        $sumInBTC = $this->startBalance[$this->config->getFirstSymbol()] * \Src\Utils\Number::wrapFloat($prices['bid'] - 0.00000001);
        $this->startBalance['totalSum'] = \Src\Utils\Number::wrapFloat(($sumInBTC - ($sumInBTC * $this->config->getConfig('fee'))) + $this->balance[$this->config->getSecondSymbol()]);

    }

    public function printBalance($result = null)
    {
        echo PHP_EOL . "You have:\n";
        foreach($this->balance as $symbol => $value)
        {
            echo $symbol . ": " .$value . PHP_EOL;
        }
    }

    public function printShortBalance($price, \Src\Config\Config $config, \Src\Binance\OrderManager $orderManager)
    {
        $str = PHP_EOL . "  You have: ";
        foreach($this->balance as $symbol => $value)
        {
            $str .= $symbol . ": " .$value;
            if($value > $this->startBalance[$symbol]){
                $str .= " (+" . number_format(100 - (100 * ($this->startBalance[$symbol] / $value)), 2). "%), ";
            } else {
                $str .= " (" . number_format(100 - (100 * ($this->startBalance[$symbol] / $value)), 2). "%), ";
            }
        }


        /**
         * With orders
         */
        $firstSymbolFromOrders = $orderManager->getPanicSellSum(\Src\Binance\OrderManager::TYPE_SELL);
        $secondSymbolFromOrder = $orderManager->getPanicSellSum(\Src\Binance\OrderManager::TYPE_BUY);


        $str .= " || Include Orders ";
        foreach($this->balance as $symbol => $value)
        {
            $value = floatval($value);

            $str .= $symbol . ": ";

            if($symbol == $this->config->getFirstSymbol())
            {
                $str .= $value + $firstSymbolFromOrders;
            } else if ($symbol == $this->config->getSecondSymbol()) {
                $str .= $value + $secondSymbolFromOrder;
            }

            if($value > $this->startBalance[$symbol]){
                $str .= " (+";
            } else {
                $str .= " (" ;
            }

            if($symbol == $this->config->getFirstSymbol())
            {
                $str .= number_format(100 - (100 * ((float)$this->startBalance[$symbol] / ((float)$value + $firstSymbolFromOrders))), 2). "%), ";
            } else if ($symbol == $this->config->getSecondSymbol()) {
                $str .= number_format(100 - (100 * ((float)$this->startBalance[$symbol] / ((float)$value + $secondSymbolFromOrder))), 2). "%), ";

            }
        }



        /**
         * Total profit
         */
        $str .= "|| Total Profit in ". $config->getSecondSymbol() .": " . $this->_getTotalProfit($price);

        /**
         * Include orders
         */
        $str .= " || Include Order ". $config->getSecondSymbol() .": " . $this->_getTotalProfitIncludeOrders($price, $orderManager);


        echo $str . PHP_EOL;
    }

    private function _getTotalProfit($price)
    {
        $str = "";
        $sumToBTC       = $this->balance[$this->config->getFirstSymbol()] * \Src\Utils\Number::wrapFloat($price - 0.00000001);
        $sum            = \Src\Utils\Number::wrapFloat(($sumToBTC - ($sumToBTC * $this->config->getConfig('fee'))) + $this->balance[$this->config->getSecondSymbol()]);

        $str .= $sum;
        if($sum > $this->startBalance['totalSum']){
            $str .= " (+" .number_format(100 - (100 * ($this->startBalance['totalSum'] / $sum)), 2). "%)";
        } else {
            $str .= " (" . number_format(100 - (100 * ($this->startBalance['totalSum'] / $sum)), 2). "%)";
        }

        return $str;
    }

    private function _getTotalProfitIncludeOrders($price, \Src\Binance\OrderManager $orderManager)
    {
        $str = "";
        $firstSymbolFromOrders = $orderManager->getPanicSellSum(\Src\Binance\OrderManager::TYPE_SELL);
        $secondSymbolFromOrder = $orderManager->getPanicSellSum(\Src\Binance\OrderManager::TYPE_BUY);

        $sumToBTC       = ($this->balance[$this->config->getFirstSymbol()] + $firstSymbolFromOrders) * \Src\Utils\Number::wrapFloat($price - 0.00000001);
        $sum            = \Src\Utils\Number::wrapFloat(($sumToBTC - ($sumToBTC * $this->config->getConfig('fee'))) + ($this->balance[$this->config->getSecondSymbol()] + $secondSymbolFromOrder));

        $str .= $sum;
        if($sum > $this->startBalance['totalSum']){
            $str .= " (+" .number_format(100 - (100 * ($this->startBalance['totalSum'] / $sum)), 2). "%)";
        } else {
            $str .= " (" . number_format(100 - (100 * ($this->startBalance['totalSum'] / $sum)), 2). "%)";
        }

        return $str;
    }

    /**
     * @return array
     */
    public function getBalances()
    {
        return $this->balance;
    }

    /**
     * @param $symbol
     * @return string
     */
    public function getBalance($symbol)
    {
        return \Src\Utils\Number::wrapFloat($this->balance[$symbol]);
    }

    /**
     * @param $symbol
     * @param $count
     * @return bool
     */
    public function incBalance($symbol, $count)
    {
        $this->balance[$symbol] = \Src\Utils\Number::wrapFloat(($this->balance[$symbol]) + ($count));
        return true;
    }

    /**
     * @param $symbol
     * @param $count
     * @return bool
     */
    public function decBalance($symbol, $count)
    {
        if($this->balance[$symbol] < $count)
        {
            return false;
        }

        $this->balance[$symbol] = \Src\Utils\Number::wrapFloat(($this->balance[$symbol]) - ($count));
        return true;
    }
}