<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 20/01/2018
 * Time: 22:59
 */

namespace Src\Binance;


class Credentials
{
    /**
     * @var string
     */
    private $key    = "";

    /**
     * @var string
     */
    private $secret = "";


    public function __construct(\Src\Config\Config $config)
    {
        $this->key      = $config->getConfig('api_key');
        $this->secret   = $config->getConfig('api_secret');
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

}