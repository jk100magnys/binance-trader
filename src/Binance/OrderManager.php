<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 13:52
 */

namespace Src\Binance;


class OrderManager
{
    const TYPE_SELL = 'sell';
    const TYPE_BUY  = 'buy';

    /**
     * @var array
     */
    private $orders = [];

    /**
     * @var string
     */
    private $minimal;

    /**
     * @var Balance
     */
    private $balance;

    /**
     * @var \Src\Config\Config
     */
    private $config;

    public function __construct(\Src\Binance\Balance $balance, \Src\Config\Config $config)
    {
        $this->config = $config;
        $this->balance = $balance;
        $this->minimal = \Src\Utils\Number::wrapFloat($this->config->getConfig('min_value_' . strtolower($this->config->getSecondSymbol())));
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Checking Order for open other one (We dont want duplicate the same conditions)
     * Checking Orders for close if target was reached
     * @param $symbol
     * @param $type
     * @param $price
     * @return bool
     */
    public function checkConditions($symbol, $type, $price)
    {
        /**
         * For Open
         */
        $bNeedToOpen = true;
        foreach($this->orders as $id => $order){
            if($order->getType() == $type && $order->getRawPrice() == $price)
            {
                $bNeedToOpen = false;
            }

            /**
             * For Close
             */
            $bCheckForClose = $order->checkForClose($price);
            if($bCheckForClose)
            {
                $this->closeOrder($id, $this->balance);
            }
        }
        if($bNeedToOpen){
            $this->createOrder($symbol, $type, $price);
        }

        return true;
    }

    /**
     * @param $type
     * @return string
     */
    public function getPanicSellSum($type)
    {
        $sum = 0.0;
        foreach($this->orders as $order)
        {

            if($order->getType() == $type)
            {
                if($type == self::TYPE_BUY)
                {
                    $sum += $order->getSum();
                }
                else if ($type == self::TYPE_SELL)
                {
                    $sum += $order->getCount();
                }
            }
        }

        return $sum;
    }

    private function _calcCount($price)
    {
        return ceil($this->minimal / $price);
    }

    public function closeOrder($id, $balance)
    {
        $this->orders[$id]->closeOrder($balance);
        unset($this->orders[$id]);
    }

    public function createOrder($symbol, $type, $price)
    {
        $count = $this->_calcCount($price);
        $this->orders[] = new \Src\Binance\Order($symbol, $type, $price, $count, $this->balance);
    }

    public function createTwoOrders($symbol, $prices)
    {
        $this->checkConditions($symbol, self::TYPE_BUY, $prices['bid']);
        $this->checkConditions($symbol, self::TYPE_SELL, $prices['ask']);

    }

    public function report()
    {
        $buyCount = 0;
        $sellCount = 0;

        foreach($this->orders as $order)
        {
            if($order->getType() == self::TYPE_SELL){ $sellCount++; }
            if($order->getType() == self::TYPE_BUY){ $buyCount++; }

        }

        echo PHP_EOL . sprintf("  We have %s buy orders and %s sell orders", $buyCount, $sellCount);
    }

}