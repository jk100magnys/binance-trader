<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 01:14
 */

namespace Src\Binance;


class Order
{
    /**
     * @var
     */
    private $symbol;

    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $price;

    /**
     * @var
     */
    private $rawPrice;

    /**
     * @var
     */
    private $count;

    /**
     * @var float
     */
    private $sum;

    /**
     * @var void
     */
    private $result;

    /**
     * @var float
     */
    private $fee = 0.01 / 10;

    public function __construct($symbol, $type, $price, $count, \Src\Binance\Balance $balance)
    {
        $this->symbol   = $symbol;
        $this->type     = $type;
        $this->count    = $count;
        $this->rawPrice = $price;

        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            $this->price = \Src\Utils\Number::wrapFloat($price + 0.00000005);
        } else {
            $this->price = \Src\Utils\Number::wrapFloat($price - 0.00000005);
        }

        $this->sum      = \Src\Utils\Number::wrapFloat($this->price  * $this->count);
        $this->result   = $this->_calcResult();

        if(!$this->_holdFunds($balance))
        {
            $this->_fundsReport($balance);
            return false;
        }

        $this->_openReport();
    }

    private function _calcResult()
    {
        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            return $this->result = \Src\Utils\Number::wrapFloat($this->count - ($this->count * $this->fee));
        }
        else if ($this->type == \Src\Binance\OrderManager::TYPE_SELL)
        {
            return $this->result = \Src\Utils\Number::wrapFloat($this->sum - ($this->sum * $this->fee));
        }
    }

    /**
     * Checking order for Close
     * @param $price
     * @return bool
     */
    public function checkForClose($price)
    {
        if(
            $this->type == \Src\Binance\OrderManager::TYPE_BUY &&
            $price <= \Src\Utils\Number::wrapFloat($this->price - 0.00000010))
        {
            return true;
        }
        else if (
            $this->type == \Src\Binance\OrderManager::TYPE_SELL &&
            $price >= \Src\Utils\Number::wrapFloat($this->price + 0.00000010))
        {
            return true;
        }


        return false;
    }

    public function closeOrder(\Src\Binance\Balance $balance)
    {
        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            $balance->incBalance('CND', $this->result);
        }
        else if($this->type)
        {
            $balance->incBalance('BTC', $this->result);
        }

        $this->_closeReport();

    }

    private function _holdFunds($balance)
    {
        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            return $balance->decBalance('BTC', ($this->price * $this->count));
        }
        else if($this->type)
        {
            return $balance->decBalance('CND', $this->count);
        }
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return void
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     *
     */
    private function _openReport()
    {
        $str = "";
        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            $str = "  == Bot want to buy %s CND by %s (Sum %s, received %s CND)";
        }
        else if ($this->type == \Src\Binance\OrderManager::TYPE_SELL)
        {
            $str = "  == Bot want to sell %s CND by %s (Sum %s, received %s BTC)";

        }

        echo PHP_EOL . sprintf($str,
                $this->count,
                $this->price,
                $this->sum,
                $this->result
            );
    }

    private function _closeReport()
    {
        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            echo PHP_EOL . sprintf("  (!) == Orders buy was closed (%s, %s, %s, %s) --- Received %s CND",
                    $this->price,
                    $this->count,
                    $this->sum,
                    $this->result,
                    $this->result
                );
        }
        else if ($this->type == \Src\Binance\OrderManager::TYPE_SELL)
        {
            echo PHP_EOL . sprintf("  (!) == Orders sell was closed (%s, %s, %s, %s) --- Received %s BTC",
                    $this->price,
                    $this->count,
                    $this->sum,
                    $this->result,
                    $this->result
                );

        }
    }

    private function _fundsReport($balance)
    {
        if($this->type == \Src\Binance\OrderManager::TYPE_BUY)
        {
            echo sprintf(" (!!!) == Not enough funds for order! Need %s BTC (available %s BTC)",
                    $this->sum,
                    $balance->getBalance("BTC")
                ) . PHP_EOL;
        }
        else if ($this->type == \Src\Binance\OrderManager::TYPE_SELL)
        {
            echo sprintf(" (!!!) == Not enough funds for order! Need %s CND (available %s CND)",
                    $this->count,
                    $balance->getBalance("CND")
                ) . PHP_EOL;

        }
    }

    /**
     * @return mixed
     */
    public function getRawPrice()
    {
        return $this->rawPrice;
    }

}