<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 14:00
 */

namespace Src\Utils;


class Number
{
    /**
     * @param $number
     * @return string
     */
    public static function wrapFloat($number)
    {
        return number_format($number, 8, ".","");
    }
}