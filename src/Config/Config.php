<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 20:42
 */

namespace Src\Config;


class Config
{
    private $config = [];

    public function __construct()
    {

        $this->config = include("config.php");
    }

    public function getConfig($key)
    {
        if($key == 'symbol')
        {
            return $this->getSymbolWithoutSlash();
        }

        return $this->config[$key];
    }

    public function getSymbolWithoutSlash()
    {
        return str_replace("/", "", $this->config['symbol']);
    }

    public function getFirstSymbol()
    {
        return explode("/", $this->config['symbol'])[0];
    }

    public function getSecondSymbol()
    {
        return explode("/", $this->config['symbol'])[1];
    }
}