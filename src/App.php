<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 21/01/2018
 * Time: 13:40
 */

namespace Src;


class App
{

    /**
     * @var Binance\Credentials
     */
    private $credentials;

    /**
     * @var Binance\API
     */
    private $api;

    /**
     * @var Binance\Balance
     */
    private $balance;

    /**
     * @var Metrics\Metrics
     */
    private $metrics;

    /**
     * @var
     */
    private $symbol;

    /**
     * @var Trade
     */
    private $trader;

    /**
     * @var Binance\OrderManager
     */
    private $orderManager;

    /**
     * @var Config\Config
     */
    private $config;

    public function __construct($symbol)
    {
        $this->config       = new \Src\Config\Config();
        $this->credentials  = new \Src\Binance\Credentials($this->config);
        $this->api          = new \Src\Binance\API($this->credentials->getKey(), $this->credentials->getSecret());
        $this->symbol       = $this->config->getConfig('symbol');

        $this->balance      = new \Src\Binance\Balance($this->config);
        $this->metrics      = new \Src\Metrics\Metrics($this->api, $symbol);
        $this->trader       = new \Src\Trade();
        $this->orderManager = new \Src\Binance\OrderManager($this->balance, $this->config);

        /**
         * for simulate
         */
        $this->balance->addBalanceForSimulate($this->metrics);
    }

    public function tick()
    {
        /**
         * Depth Metric
         */
        $depthGoal = $this->metrics->getDepthMetric()->checkGoalSimulate();
        echo $this->metrics->getDepthMetric()->printDepth();
        $depthData = $this->metrics->getDepthMetric()->getResult();
        if($depthGoal)
        {
            $this->orderManager->createTwoOrders($this->symbol, $depthData);
        }

        $this->orderManager->report();
        $this->balance->printShortBalance($depthData['bid'], $this->config, $this->orderManager);
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return Trade
     */
    public function getTrader()
    {
        return $this->trader;
    }

    /**
     * @return Binance\Balance
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return Metrics\Metrics
     */
    public function getMetrics()
    {
        return $this->metrics;
    }
}