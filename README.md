# Bot trader for Binance
Bot can checking Depth for some Symbol and calculate small different between Bid ans Ask.
Then Bot can make decision about buy/sell (scalping method)


# API

Api used by Binance

[![GitHub last commit](https://img.shields.io/github/last-commit/jaggedsoft/php-binance-api.svg)](#)
[![Packagist Downloads](https://img.shields.io/packagist/dt/jaggedsoft/php-binance-api.svg)](https://packagist.org/packages/jaggedsoft/php-binance-api)

This project is designed to help you make your own projects that interact with the [Binance API](https://www.binance.com/restapipub.html). You can stream candlestick chart data, market depth, or use other advanced features such as setting stop losses and iceberg orders. This project seeks to have complete API coverage including WebSockets.
