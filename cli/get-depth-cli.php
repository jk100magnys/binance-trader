<?php
/**
 *
 *  Preparing
 *
 */
system('clear');
require 'vendor/autoload.php';

/**
 *
 *  Definition
 *
 */
$symbol = "CNDBTC";

if (php_sapi_name() == "cli") {
    $colors = new \Src\Utils\Colors();
    echo PHP_EOL . $colors->getColoredString("======  TRADING DEPTH FOR ".$symbol."  =========", 'yellow', 'black') . PHP_EOL . PHP_EOL;

} else {
    echo PHP_EOL."======  TRADING DEPTH FOR ".$symbol."  =========".PHP_EOL.PHP_EOL;
}

$app = new \Src\App($symbol);
$app->getBalance()->printBalance($app->getMetrics()->getDepthMetric()->getResult());

while(true) {

    $app->tick();

    echo PHP_EOL;
}

?>