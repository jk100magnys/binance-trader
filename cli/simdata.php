<?php
$q = 0.00001728;

$result = [];

for($t = 1; $t <= 500; $t++)
{
    $rand = rand(0, 9);
    $randOp = rand(0,1);

    if($randOp == 0){
        $q += floatval("0.0000000".$rand);
    } else {
        $q -= floatval("0.0000000".$rand);
    }

    $randDiff = rand(1, 9);
    $s = $q + floatval("0.0000000".$randDiff);

    $result[] = ['bids' => [number_format($q, 8, ".", "") => 0], 'asks' => [number_format($s, 8, ".", "") => 0]];
}


return $result;
?>